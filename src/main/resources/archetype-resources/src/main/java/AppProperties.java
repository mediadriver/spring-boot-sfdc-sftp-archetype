#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// Salesforce details
	@Value("${symbol_dollar}{sfdc.loginUrl}")
	private String sfdcLoginUrl;

	@Value("${symbol_dollar}{sfdc.clientId}")
	private String sfdcClientId;

	@Value("${symbol_dollar}{sfdc.clientSecret}")
	private String sfdcClientSecret;

	@Value("${symbol_dollar}{sfdc.userName}")
	private String sfdcUserName;

	@Value("${symbol_dollar}{sfdc.password}")
	private String sfdcPassword;

	// Route details
	@Value("${symbol_dollar}{salesforce.action}")
	private String salesforceAction;
	
	@Value("${symbol_dollar}{secure.ftp.url}")
	private String secureFtpUrl;

	public String getSfdcLoginUrl() {
		return sfdcLoginUrl;
	}

	public void setSfdcLoginUrl(String sfdcLoginUrl) {
		this.sfdcLoginUrl = sfdcLoginUrl;
	}

	public String getSfdcClientId() {
		return sfdcClientId;
	}

	public void setSfdcClientId(String sfdcClientId) {
		this.sfdcClientId = sfdcClientId;
	}

	public String getSfdcClientSecret() {
		return sfdcClientSecret;
	}

	public void setSfdcClientSecret(String sfdcClientSecret) {
		this.sfdcClientSecret = sfdcClientSecret;
	}

	public String getSfdcUserName() {
		return sfdcUserName;
	}

	public void setSfdcUserName(String sfdcUserName) {
		this.sfdcUserName = sfdcUserName;
	}

	public String getSfdcPassword() {
		return sfdcPassword;
	}

	public void setSfdcPassword(String sfdcPassword) {
		this.sfdcPassword = sfdcPassword;
	}

	public String getSalesforceAction() {
		return salesforceAction;
	}

	public void setSalesforceAction(String salesforceAction) {
		this.salesforceAction = salesforceAction;
	}

	public String getSecureFtpUrl() {
		return secureFtpUrl;
	}

	public void setSecureFtpUrl(String secureFtpUrl) {
		this.secureFtpUrl = secureFtpUrl;
	}

}
